import json
import sys
import argparse
import pandas as pd
pd.set_option('display.max_colwidth', -1)

def get_params(argv):
    parser = argparse.ArgumentParser(description='Pipeline')
    parser.add_argument('-db', '--db', help="JSON file directory", required=True)
    parser.add_argument('-do', '--do', help="create/modify/getcsv", required=True)
    parser.add_argument('-var', '--var', help="Var to modify/create", required=False, default=1000)
    parser.add_argument('-value', '--value', help="Value to assign the variable to", required=False, default=0)
    parser.add_argument('-o', '--o', help="Output for csv, if -do='getcsv'", required=False, default='db.csv')
    a = parser.parse_args()
    return a


def modifyVal(data, path, value):
    variables=[ele for ele in path.split(".") if ele!=""]
    if len(variables)==1:
        data[variables[0]]=value
    elif len(variables)==2:
        data[variables[0]][variables[1]]=value
    elif len(variables)==3:
        data[variables[0]][variables[1]][variables[2]]=value
    elif len(variables)==4:
        data[variables[0]][variables[1]][variables[2]][variables[3]]=value
        
        
def writeWebsite(data,a):
    df=pd.DataFrame(data)
    df=df.T
    df['website']="<a target='_blank' href='"+df['url']+"'>Link</a>"
    df = df[['batch','species','strain','jgi_id','host','sampled_in','soil','website','phylum','class','order']]
    with open(a.db.replace('.json','.html'),'w+') as htmlfile:
        html="<!doctype html><head><meta charset='utf-8'><title>JGI 42 Endophytes</title><style>table {white-space: nowrap;}</style><link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css'/><script src='https://code.jquery.com/jquery-3.3.1.min.js' type='text/javascript'></script><script type='text/javascript' src='https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js'></script><script type='text/javascript'>$( document ).ready(function() {$('.dataframe').DataTable({'paging': false,'select': true});})</script><body><center><h1>42 endophytic fungi genomes</h1></center>"
        html=html+df.to_html()
        html=html+"</body>"
        html=html.replace("&lt;",'<').replace('&gt;','>')
        htmlfile.write(html)
        
        
def getCSV(data,a):
    df=pd.DataFrame(data)
    df=df.T
    df = df[['batch','species','strain','jgi_id','host','sampled_in','soil','phylum','class','order']]
    df.to_csv(a.o)



if __name__ == '__main__':
    a = get_params(sys.argv[1:])

    with open(a.db, "r") as jsonFile:
        data = json.load(jsonFile)
    
    if a.do=='create' or a.do=='modify':
        if a.do=="create":
            modifyVal(data, a.var, {})
        elif a.do=="modify":
            modifyVal(data, a.var, a.value)

        with open(a.db, "w") as jsonFile:
            json.dump(data, jsonFile)

        writeWebsite(data,a)

        print('DONE')
        
    elif a.do=='getcsv':
        getCSV(data,a)
        print('DONE')
    else:
        print('ERROR: Unrecognized --do variable (must be modify|create|getcsv)')

