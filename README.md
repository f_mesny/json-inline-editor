
This script allows to modify the content of a JSON file using command lines.
It also assembles an html page that displays your data as a table, if possible.

### Examples

#### To add a new sub-object:

We can run the following command
```
python /programme_directory/json_editor.py -db /working_directory/your_db.json -do create -var new_object
```
(In an empty your_db object, this would create 'new_object':{})

To create an object in this 'new_object':

```
python /programme_directory/json_editor.py -db /working_directory/your_db.json -do create -var new_object.new_object2
```
(In your 'your_db' object, you would then have 'new_object':{'new_object2':{}})

- To modify the value of a variable:


#### To modify or add a value in the database:

You can simply do: 
```
python /programme_directory/json_editor.py -db /working_directory/your_db.json -do modify -var new_object.value1 -value 841
```
This would result in the following database: {'new_object':{'new_object2':{}, 'value1': 841})}

#### To export the database as a CSV file:

You can simply do: 
```
python /programme_directory/json_editor.py -db /working_directory/your_db.json -do getcsv -o output.csv
```


